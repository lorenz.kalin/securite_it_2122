/*

    Sources: https://resources.infosecinstitute.com/topic/cbc-byte-flipping-attack-101-approach/

    Challenge: nc challenge01.root-me.org 51035

    1) Account Creation
    2) Login
    3) Leave
    >>1
    [?] Name : asdf
    [?] Mail address (To receive alert when new exploits are published) :   asdffdsa
    [*] Let's generate the authentification token....
    1)--> First, a plaintext token is generated :
    |       b'[id=546815648;name=asdf;is_member=false;mail=asdffdsa;pad=00000]'
    2)--> Second, the token is encrypted with military grade encryption (AES CBC)
    |       AES_CBC(<Secret_key>,<IV>,b'[id=5468 15648;name=asdf;is_member=false;mail=asdffdsa;pad=00000]')
    |       Your token is : 'IRZjBh6G xjeYI7YZvxwfBAtvoqr7GvBM1QsrODSipKj5LUSKae7ZTnl6xQEJQUgXPKzEx7hviZ+j60xh4VLv9w=='
    |       Save it to be able to login later...
    1) Account Creation
    2) Login
    3) Leave
    >>

    openssl enc -base64 -d -in t.b64 -out t.bin

    [lmi@fedora TP02]$ hexdump -C t.bin    
    00000000  21 16 63 06 1e 86 c6 37  98 23 b6 19 bf 1c 1f 04  |!.c....7.#......|
    00000010  0b 6f a2 aa fb 1a f0 4c  d5 0b 2b 38 34 a2 a4 a8  |.o.....L..+84...|
    00000020  f9 2d 44 8a 69 ee d9 4e  79 7a c5 01 09 41 48 17  |.-D.i..Nyz...AH.|
    00000030  3c ac c4 c7 b8 6f 89 9f  a3 eb 4c 61 e1 52 ef f7  |<....o....La.R..|
    00000040
    
    [id=546815648;na me=asdf;is_membe r=false;mail=asd ffdsa;pad=00000]

    [id=546815648;name=asdf;is_member=true;mail=asdffdsa;pad=00000]         <--Goal

    IRZjBh6G xjeYI7YZ vxwfBAtv oqr7GvBM 1QsrODSi pKj5LUSK ae7ZTnl6 xQEJQUgX PKzEx7hv iZ+j60xh 4VLv9w==
                      ^
                      |
                     0x76 ^ 0x0b = 01110110 ^ 00001011 = 01111101 = 0x7D

*/

#include <stdio.h>

#define BUFFER_SIZE 16

int main(){

    unsigned char buffer[BUFFER_SIZE];
    unsigned char test[] = {0x21, 0x16, 0x63, 0x06, 0x1e, 0x86, 0xc6, 0x37,
                            0x98, 0x23, 0xb6, 0x19, 0xbf, 0x1c, 0x1f, 0x04};
    unsigned char test2[] = {0x0b, 0x6f, 0xa2, 0xaa, 0xfb, 0x1a, 0xf0, 0x4c,
                            0xd5, 0x0b, 0x2b, 0x38, 0x34, 0xa2, 0xa4, 0xa8};

    FILE *in_file;
    FILE *out_file;

    in_file = fopen("t.b64", "rb");
    out_file = fopen("t_xor.bin", "wb");

    fread(buffer, BUFFER_SIZE, 1, in_file);         //read the first 16 bytes of encrypted text to buffer

    for (int i = 0; i < 16; i++){
        buffer[i] ^= test2[i];
    }

    fwrite(buffer, 1, BUFFER_SIZE, out_file);       //write buffer to file

    for (int i = 0; buffer[i] != '\0'; i++){
        printf("%c", buffer[i]);
    }


    

    fclose(in_file);
    fclose(out_file);


    //----Tests----

    // FILE *input_file;
    // FILE *output_file;

    // input_file = fopen("token.txt", "rb");
    // output_file = fopen("token_decoded.bin", "wb");

    // unsigned char buffer[BUFFER_SIZE];
    // fseek(input_file, SEEK_POS, SEEK_CUR);
    // fread(buffer, sizeof(char), BUFFER_SIZE, input_file);
    
    // fwrite(buffer, 1, 16, output_file);

    // for (int i = 0; buffer[i] != '\0'; i++)
    // {
    //     printf("%c", buffer[i]);
    // }

    // fclose(input_file);
    // fclose(output_file);
}
