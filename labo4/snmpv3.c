#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>

#define SIZE_MD5 16

unsigned char * hash(unsigned char *input, int length);
int auth(unsigned char *pw);

//-------------------------------------------------------------------------

/*
https://www.root-me.org/de/Challenges/Netzwerk/SNMP-Authentification

ch16.pcap, trame 4

Sources:
https://stackoverflow.com/questions/5248915/execution-time-of-c-program, 26.10.2021
https://vad3rblog.wordpress.com/2017/09/11/snmp/, 26.10.2021


Get Length of dictionary file
// ┌──(kali㉿kali)-[~/Documents/labo4]
    // └─$ wc -l EN\ -\ rootmedict.txt                                                                                  
    // 109863 EN - rootmedict.txt

*/
// unsigned char wholeMsg[132] = {0x30, 0x81, 0x81, 0x02, 0x01, 0x03, 0x30, 0x11, 0x02, 0x04, 0x20, 0xdd, 0x06, 0xa7, 0x02, 0x03, 0x00, 0xff, 0xe3, 0x04, 0x01, 0x01, 0x02, 0x01, 0x03, 0x04, 0x31, 0x30, 0x2f, 0x04, 0x11, 0x80, 0x00, 0x1f, 0x88, 0x80, 0xe9, 0xbd, 0x0c, 0x1d, 0x12, 0x66, 0x7a, 0x51, 0x00, 0x00, 0x00, 0x00, 0x02, 0x01, 0x05, 0x02, 0x01, 0x20, 0x04, 0x04, 0x75, 0x73, 0x65, 0x72, 0x04, 0x0c, 0x09, 0x44, 0x5f, 0x87, 0x77, 0x0f, 0x57, 0x52, 0x21, 0x33, 0x1c, 0x7c, 0x04, 0x00, 0x30, 0x36, 0x04, 0x11, 0x80, 0x00, 0x1f, 0x88, 0x80, 0xe9, 0xbd, 0x0c, 0x1d, 0x12, 0x66, 0x7a, 0x51, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0xa2, 0x1f, 0x02, 0x04, 0x6b, 0x4c, 0x5a, 0xc2, 0x02, 0x01, 0x00, 0x02, 0x01, 0x00, 0x30, 0x11, 0x30, 0x0f, 0x06, 0x0a, 0x2b, 0x06, 0x01, 0x02, 0x01, 0x04, 0x1e, 0x01, 0x05, 0x02, 0x02, 0x01, 0x01};

unsigned char editedWholeMsg[132] = {0x30, 0x81, 0x81, 0x02, 0x01, 0x03, 0x30, 0x11, 0x02, 0x04, 0x20, 0xdd, 0x06, 0xa7, 0x02, 0x03,
                                    0x00, 0xff, 0xe3, 0x04, 0x01, 0x01, 0x02, 0x01, 0x03, 0x04, 0x31, 0x30, 0x2f, 0x04, 0x11, 0x80,
                                    0x00, 0x1f, 0x88, 0x80, 0xe9, 0xbd, 0x0c, 0x1d, 0x12, 0x66, 0x7a, 0x51, 0x00, 0x00, 0x00, 0x00,
                                    0x02, 0x01, 0x05, 0x02, 0x01, 0x20, 0x04, 0x04, 0x75, 0x73, 0x65, 0x72, 0x04, 0x0c, 0x00, 0x00,
                                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x30, 0x36, 0x04, 0x11,
                                    0x80, 0x00, 0x1f, 0x88, 0x80, 0xe9, 0xbd, 0x0c, 0x1d, 0x12, 0x66, 0x7a, 0x51, 0x00, 0x00, 0x00,
                                    0x00, 0x04, 0x00, 0xa2, 0x1f, 0x02, 0x04, 0x6b, 0x4c, 0x5a, 0xc2, 0x02, 0x01, 0x00, 0x02, 0x01,
                                    0x00, 0x30, 0x11, 0x30, 0x0f, 0x06, 0x0a, 0x2b, 0x06, 0x01, 0x02, 0x01, 0x04, 0x1e, 0x01, 0x05,
                                    0x02, 0x02, 0x01, 0x01};

unsigned char msgAuthorativeEngineID[] = {0x80, 0x00, 0x1f, 0x88, 0x80, 0xe9, 0xbd, 0x0c, 0x1d, 0x12, 0x66, 0x7a, 0x51, 0x00, 0x00, 0x00, 0x00};
unsigned char msgAuthenticationParameters[] = {0x09, 0x44, 0x5f, 0x87, 0x77, 0x0f, 0x57, 0x52, 0x21, 0x33, 0x1c, 0x7c};

//-------------------------------------------------------------------------

int main(){

    clock_t begin = clock();

    FILE *pFile;
    pFile = fopen("EN - rootmedict.txt", "rb");

    size_t lengthBuffer = 128;
    size_t characters;
    int i = 1;

    while (i < 109863){
        
        unsigned char *lineBuffer = calloc(lengthBuffer, sizeof(unsigned char));
        char *b = lineBuffer;
        getline(&b, &lengthBuffer, pFile);
        // printf("%s", lineBuffer);
        printf("------------------------------------------\n");
        printf("Try n°: %d\n", i);
        if(auth(lineBuffer) == 1)
            break;
        i++;
        free(lineBuffer);
    }

    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    int s = (int)(i/time_spent);
    int min = (int)(s*60);
    printf("Took %.4f seconds; %d/s, %d/min\n", time_spent, s, min);
    printf("------------------------------------------\n");
    fclose(pFile);

    // Test with actual password:
    // auth("4dm1n\n");

    return 0;
}

int auth(unsigned char *pw){

    unsigned char *password = pw;
    int lengthPassword = strlen(password);
    // lenghtPassword -1 because of '\n' character
    printf("Length of password: %d\n", lengthPassword -1);
    printf("Password is: ");
    for (int i = 0; i < lengthPassword -1; i++){
        printf("%c", password[i]);
    }
    printf("\n");

    // 1. STEP: Create "string1" by repeating the password until its 1048576 bytes long, truncating as needed
    unsigned char string1[1048576] = {0x00};
    int lengthString1 = sizeof(string1)/sizeof(string1[0]);
    for (int i = 0; i < lengthString1; i++){
        // lenghtPassword -1 because of '\n' character
        string1[i] = password[i % (lengthPassword -1)];
        // printf("%c", string1[i]);
    }
    // printf("\n");

    // 2. STEP: Create "digest1"
    unsigned char *p0;
    p0 = hash(string1, lengthString1);

    unsigned char digest1[16] = {0x00};
    int lengthDigest1 = sizeof(digest1)/sizeof(digest1[0]);
    for (int i = 0; i < lengthDigest1; i++){
        digest1[i] = p0[i];
    }

    // 3. STEP: Create "string2" by combining digest1 + msgAuthorativeEngineID + digest1
    int lengthMsgAuthorativeEngineID = sizeof(msgAuthorativeEngineID)/sizeof(msgAuthorativeEngineID[0]);
    int lengthString2 = lengthDigest1 + lengthMsgAuthorativeEngineID + lengthDigest1;
    unsigned char string2[lengthString2];
    memset(&string2[0], 0x00, sizeof(string2));
    for (int i = 0; i < lengthString2; i++){
        if(i < lengthDigest1){
            string2[i] = digest1[i];
        }else if (i < (lengthDigest1 + lengthMsgAuthorativeEngineID)){
            string2[i] = msgAuthorativeEngineID[i - lengthDigest1];
        }else{
            string2[i] = digest1[i - (lengthDigest1 + lengthMsgAuthorativeEngineID)];
        }
        // printf("%.2x", string2[i]);
    }
    // printf("\n");


    // 4. STEP: Create "authKey" (digest of string2)
    unsigned char *p1;
    p1 = hash(string2, lengthString2);

    unsigned char authKey[16] = {0x00};
    int lengthAuthKey = sizeof(authKey)/sizeof(authKey[0]);
    // printf("AuthKey length: %d\n", lengthAuthKey);
    // printf("Returned hash value (AuthKey): ");
    for (int i = 0; i < lengthAuthKey; i++){
        authKey[i] = p1[i];
        // printf("%.2x", authKey[i]);
    }
    printf("\n");

    // 5. STEP: Create extendedAuthKey
    unsigned char extendedAuthKey[64];
    memset(&extendedAuthKey[0], 0, sizeof(extendedAuthKey));
    for (int i = 0; i < 64; i++){
        if (i < lengthAuthKey){
            extendedAuthKey[i] = authKey[i];
        }else{
        extendedAuthKey[i] = 0x00;
        }
    }

    // 6. STEP: Create ipad and K1
    unsigned char ipad[64] = {0x00};
    for (int i = 0; i < 64; i++){
        ipad[i] ^= 0x36;
    }

    unsigned char k1[64] = {0x00};
    int lengthK1 = sizeof(k1)/sizeof(k1[0]);
    // printf("\nLength K1: %d\n", lengthK1);
    // printf("K1: ");
    for (int i = 0; i < 64; i++){
        k1[i] = ipad[i] ^ extendedAuthKey[i];
        // printf("%.2x", k1[i]);
    }

    // 7. STEP: Create ipad and K2
    unsigned char opad[64] = {0x00};
    for (int i = 0; i < 64; i++){
        opad[i] ^= 0x5c;
    }

    unsigned char k2[64] = {0x00};
    int lengthK2 = sizeof(k2)/sizeof(k2[0]);
    // printf("\nLength k2: %d\n", lengthK1);
    // printf("K2: ");
    for (int i = 0; i < 64; i++){
        k2[i] = opad[i] ^ extendedAuthKey[i];
        // printf("%.2x", k2[i]);
    }

    // 8. STEP: Create extK1 (K1 + wholeMsgMod)
    int lengthWholeMsgMod = sizeof(editedWholeMsg)/sizeof(editedWholeMsg[0]);

    int lengthExtK1 = lengthK1 + lengthWholeMsgMod;
    // printf("\nLength extK1: %d\n", lengthExtK1);
    unsigned char extK1[lengthExtK1];
    memset(&extK1[0], 0, sizeof(extK1));

    for (int i = 0; i < lengthK1; i++){
        extK1[i] ^= k1[i];
        // printf("%.2x", extK1[i]);
    }

    for (int i = lengthK1; i < lengthExtK1; i++){
        extK1[i] ^= editedWholeMsg[i -lengthK1];
    }

    // 9. STEP: Create hashK1 (digest of extK2)
    unsigned char hashK1[16];
    int lengthHashK1 = sizeof(hashK1)/sizeof(hashK1[0]);

    unsigned char *p2;
    p2 = hash(extK1, lengthExtK1);

    for (int i = 0; i < lengthHashK1; i++){
        hashK1[i] = p2[i];
        // printf("%.2x", hashK1[i]);
    }
    // printf("\n");

    // 10. STEP: Create extK2 (K2 + hashK1)
    int lengthExtK2 = lengthK2 + lengthHashK1;
    // printf("Length extK2: %d\n", lengthExtK2);
    unsigned char extK2[lengthExtK2];
    memset(&extK2[0], 0, sizeof(extK2));

    for (int i = 0; i < lengthK2; i++){
        extK2[i] ^= k2[i];
        // printf("%.2x", extK2[i]);
    }

    for (int i = lengthK2; i < lengthExtK2; i++){
        extK2[i] ^= hashK1[i -lengthK2];
    }

    // 11. STEP:Create hashK2 (digest of extK2)
    unsigned char hashK2[16];
    int lengthHashK2 = sizeof(hashK2)/sizeof(hashK2[0]);

    unsigned char *p3;
    p3 = hash(extK2, lengthExtK2);

    for (int i = 0; i < lengthHashK2; i++){
        hashK2[i] = p3[i];
    }

    // 12. STEP: Truncate first 12 bytes of hashK2
    unsigned char msgAP[12] = {0x00};
    int lengthMsgAP = sizeof(msgAP)/sizeof(msgAP[0]);
    printf("Truncated hashK2 (12 bytes): ");
    for (int i = 0; i < lengthMsgAP; i++){
        msgAP[i] = hashK2[i];
        printf("%.2x", msgAP[i]);
    }
    printf("\n");

    // Check if calculated and truncated msgAuthentificationParameters (msgAP) are same
    int status = 1;
    for (int i = 0; i < lengthMsgAP; i++){
        if (msgAP[i] == msgAuthenticationParameters[i]){
            continue;
        }else{
            status = -1;
            printf("No match! :(, status: %d\n", status);
            return status;
        }
    }
    printf("Success! :), status: %d\n", status);

    return status;
}

unsigned char * hash(unsigned char *input, int length){

    unsigned char  inputBuffer[2000000];
    int            nbByteRead = length;
    unsigned int   md_len=0;
    EVP_MD_CTX    *ctx;

    static unsigned char md_value[SIZE_MD5];

    for (int i = 0; i < length; i++){
        inputBuffer[i] = input[i];
    }

    ctx=EVP_MD_CTX_new();

    EVP_MD_CTX_init(ctx);
    EVP_DigestInit(ctx, EVP_md5());

    memset (&md_value[0], 0, sizeof(md_value));

    EVP_DigestUpdate(ctx, &inputBuffer[0], nbByteRead);

    EVP_DigestFinal(ctx, &md_value[0], &md_len);
    EVP_MD_CTX_free(ctx);

    return md_value;
}