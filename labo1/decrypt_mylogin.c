#include <stdio.h>
#include <openssl/evp.h>
#include <openssl/err.h>

//Sources:
// https://wiki.openssl.org/index.php/EVP_Symmetric_Encryption_and_Decryption#Decrypting_the_Message

//Method declaration
int decrypt(unsigned char *key_buffer, int ciphertext_len, unsigned char *key,
            unsigned char *iv, unsigned char *plaintext);

int main(){

    FILE *ciphertext;
    ciphertext = fopen("mylogin.cnf", "rb");

    unsigned char decryptedtext[128];

    unsigned char key_buffer[20];
    int ciphertext_len = 20;
    int decryptedtext_len;
    unsigned char key[16] = { 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00 };
    unsigned char iv[16];

    //Search and decode the key
    if(fseek(ciphertext, 4, SEEK_SET) == 0){

        fread(key_buffer, sizeof(char), 20, ciphertext);
        for (int i = 0; i < 20; i++) {
		    key[i % 16] ^= key_buffer[i];
            printf("key_buffer at %i is %c\n",i,key_buffer[i]);
	    }

        // Decrypt the ciphertext 
        decryptedtext_len = decrypt(key_buffer, ciphertext_len, key, iv,
                                decryptedtext);

        // Add a NULL terminator. We are expecting printable text
        decryptedtext[decryptedtext_len] = '\0';

        // Show the decrypted text
        printf("Decrypted text is:\n");
        printf("%s\n", decryptedtext);

        return 0;
        }
    
    printf("Something went wrong with fseek.\n");
    fclose(ciphertext);
    return (0);
}

int decrypt(unsigned char *key_buffer, int ciphertext_len, unsigned char *key,
            unsigned char *iv, unsigned char *plaintext){

    EVP_CIPHER_CTX *ctx;

    int len;
    int plaintext_len;
    

    //Create and initialize the context
    if(!(ctx = EVP_CIPHER_CTX_new())){
        printf("Something went wrong with the creation and initialisation\n");
    }
        
    //Initialise the decryption operation
    if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, iv)){
        printf("Something went wrong with the initialisation of the decryption operation\n");
    }
    
    /*
     * Provide the message to be decrypted, and obtain the plaintext output.
     * EVP_DecryptUpdate can be called multiple times if necessary.
     */
    if(1 != EVP_DecryptUpdate(ctx, plaintext, &len, key_buffer, ciphertext_len))
        printf("Something went wrong with the decryption\n");
    plaintext_len = len;

    /*
     * Finalise the decryption. Further plaintext bytes may be written at
     * this stage.
     */
    if(1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len))
        printf("Something went wrong with Finalize decryption\n");
    plaintext_len += len;

    // Clean up
    EVP_CIPHER_CTX_free(ctx);

    return plaintext_len;
}
